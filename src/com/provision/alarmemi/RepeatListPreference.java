package com.provision.alarmemi;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class RepeatListPreference extends CustomListPreference {
    // Initial value that can be set with the values saved in the database.
    private static Alarm.DaysOfWeek mDaysOfWeek = new Alarm.DaysOfWeek(0);
    // New value that will be set if a positive result comes back from the
    // dialog.
    private static Alarm.DaysOfWeek mNewDaysOfWeek = new Alarm.DaysOfWeek(0);
    boolean[] checkedDaysOfWeek;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position,
                                    long id) {
                if (position == 7) {
                    if (!listView.isItemChecked(7)) {
                        boolean flag = false;
                        for (int i = 0; i < 7; i++) {
                            if (listView.isItemChecked(i)) flag = true;
                        }
                        listView.setItemChecked(7, !flag);
                    } else {
                        for (int i = 0; i < 7; i++) {
                            checkedDaysOfWeek[i] = false;
                            mNewDaysOfWeek.set(i, checkedDaysOfWeek[i]);
                            listView.setItemChecked(i, false);
                        }
                    }
                } else {
                    checkedDaysOfWeek[position] = !checkedDaysOfWeek[position];
                    mNewDaysOfWeek.set(position, checkedDaysOfWeek[position]);
                    boolean flag = false;
                    for (int i = 0; i < 7; i++) {
                        if (listView.isItemChecked(i)) flag = true;
                    }
                    listView.setItemChecked(7, !flag);
                }
            }
        });
        findViewById(R.id.ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mDaysOfWeek.set(mNewDaysOfWeek);
                SetAlarm.setRepeatSummary(mDaysOfWeek.toString(
                        RepeatListPreference.this, true));
                SetAlarm.isChanged = true;
                finish();
            }
        });
    }

    @Override
    public void setDefault(String value) {
        Log.d("hello", "setDefault");
        boolean[] values = checkedDaysOfWeek = mDaysOfWeek.getBooleanArray();
        boolean flag = false;
        for (int i = 0; i < values.length; i++) {
            if (values[i]) flag = true;
            listView.setItemChecked(i, values[i]);
        }
        if (!flag) listView.setItemChecked(7, true);

    }

    public static void setDaysOfWeek(Context context, Alarm.DaysOfWeek dow) {
        mDaysOfWeek.set(dow);
        mNewDaysOfWeek.set(dow);
        SetAlarm.setRepeatSummary(dow.toString(context, true));
    }

    public static Alarm.DaysOfWeek getDaysOfWeek() {
        return mDaysOfWeek;
    }
}
